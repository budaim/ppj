#!/bin/bash

function doit() {
  ./generator < $t/test.lan
  g++ analizator/*.cpp -o analizator/analizator
  time ./analizator/analizator < $t/test.in > outf
}

for t in test/1labos-2011-2012/*
do
  echo "test $t:"
  time doit
  diff -qb $t/test.out outf
done
