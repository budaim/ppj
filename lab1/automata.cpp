#include "automata.h"
#include "utils.h"

#include <iostream>
#include <cstdio>
#include <algorithm>


// stvori novo stanje
inline State newState() { 
   return new State_(); 
}

// stvori "komad automata"
inline Part newPart() { 
   return new Part_(newState(), newState()); 
}

// je li znak ch terminal?
bool is_term(char ch) {
   return ch != '*' && 
          ch != '|' && 
	  ch != '(' && 
	  ch != ')' && 
	  ch != '{' && 
	  ch != '}';
}


// pravilo za izraz oblika x* (kleeneov operator)
void kleene(Part x) {
   x->start->add_transition(0, x->finish);
   x->finish->add_transition(0, x->start);
}

// pravilo za izraz oblika xy (operator konkatenacije, "i")
void concat(Part x, Part y) {
   x->finish->add_transition(0, y->start);
   x->finish = y->finish;
}


// pravilo za izraz oblika x|y (operator izbora, "ili")
void makeor(Part x, Part y) {
   x->start->add_transition(0, y->start);
   y->finish->add_transition(0, x->finish);
}


// konstruktor iz regexa
Automata::Automata(std::string regex) {
   pretprocess(regex);
   this->all = parse(0, (int)this->regex.length());
   clear();
}

// razrijesi koji su znakovi terminalni, koji posebni i te stvari
void Automata::pretprocess(std::string regex) {
   bool escape = false;
   for (int i = 0; i < regex.length(); ++i) {
      int ch = regex[i];

      if (escape) {
	 switch (ch) {
	    case 'n':
	       this->regex += '\n';
	       break;
	    case 't':
	       this->regex += '\t';
	       break;
	    case '_':
	       this->regex += ' ';
	       break;
	    default:
	       this->regex += ch;
	 }

	 term.push_back(true);
	 escape = false;
      } else if (ch == '\\') {
	 escape = true;
      } else {
	 this->regex += ch == '$' ? 0 : ch;
	 term.push_back(is_term(ch));
      }
   }
}

// pronadji pojavljivanje znaka target u podstringu [from, to)
// na kojem se podstring moze prelomiti (jer su zagrade lijevo od
// te pozicije balansirane

int Automata::find_balanced(int from, int to, char target) {
   int lvl = 0; 
   int ret = -1;

   for (int i = from; i < to && ret == -1; ++i) {
      if (!term[i] && regex[i] == '(') ++lvl;
      if (!term[i] && regex[i] == ')') --lvl;

      if (!term[i]            && 
	  regex[i] == target  && 
	  lvl == 0) ret = i;
   }

   return ret;
}

// parsiranje regexa

Part Automata::parse(int from, int to) {
   int orindex = find_balanced(from, to, '|'); 

   // ako se moze razbiti po znaku '|'
   if (orindex != -1) {
      // parsiraj polovice rekurzivno
      Part left = parse(from, orindex);
      Part rajt = parse(orindex + 1, to);
      // spoji dijelove automata
      makeor(left, rajt);
      return left;
   }

   // niz se ne moze razbiti po znaku '|'
   Part p = NULL; 

   // ako pocinjemo sa zagradom
   if (!term[from] && regex[from] == '(') {
      // parsiraj sve do zatvorene zagrade
      int close = find_balanced(from, to, ')');
      p = parse(from + 1, close);
      from = close;
   } else {
      // inace imamo terminalni znak (iz abecede), pa samo dodaj prijelaz
      p = newPart();
      p->start->add_transition(regex[from], p->finish);
   }

   // u slucaju zvjezdice primijeni kleene-ov operator
   if (from + 1 < regex.length() && 
       regex[from + 1] == '*'    &&
       !term[from + 1]) {
      kleene(p);
      ++from;
   }

   // parsiraj ostatak i primijeni konkatenaciju (nizanje)...
   if (from + 1 < to) concat(p, parse(from + 1, to));
   return p;
}

// neko ispisivanje

void Automata::output() {
   std::map<State, int> p;
   _output(p, all->start);
}

void Automata::_output(std::map<State, int> &p, State s) {
   p[s] = (int)p.size();

   for (int i = 0; i < s->nxt.size(); ++i) {
      if (!p.count(s->nxt[i]))   
         _output(p, s->nxt[i]);
   }

   std::cout << "state: " << p[s] << '\n';
   for (int i = 0; i < s->let.size(); ++i) {
      std::cout << (char)s->let[i] << " -> " << p[s->nxt[i]] << '\n';
   }

   std::cout << '\n';
}

// vrati se na pocetni skup stanja
void Automata::clear() {
   curr.clear();

   // treba uracunati pocetno stanje i sva stanja dohvatljiva
   // pomocu epsilon prijelaza
   std::set<State> v = get_eps(all->start);
   curr.insert(v.begin(), v.end());
}

// trazi sva stanja koja se mogu posjetiti iz stanja s
// koristeci epsilon prijelaze
std::set<State> Automata::get_eps(State s) { 
   std::set<State> ret;
   ret.insert(s);

   std::queue<State> q;
   for (q.push(s); !q.empty(); q.pop()) {
      State x = q.front();

      for (int i = 0; i < x->let.size(); ++i) {
         if (!x->let[i] && !ret.count(x->nxt[i])) {
            q.push(x->nxt[i]);
            ret.insert(x->nxt[i]);
         }
      }
   }

   return ret;
}

// je li skup stanja prazan, tj. jesmo li u stanju
// iz kojeg vise ne mozemo prihvatiti?
bool Automata::lost() {
  return curr.empty();
}

// obradi znak c, vrati true ako je prihvacen
bool Automata::next(char c) {
   // sljedeci skup stanja
   std::vector<State> nxt;

   // prodji po trenutnom skupu stanja i izracunaj sljedeci
   for (std::set<State>::iterator it = curr.begin(); it != curr.end(); ++it) {
      State s = *it;
      for (int i = 0; i < s->let.size(); ++i) {
         // trazi prijelaz sa znakom c!
         if (s->let[i] == c) { 
            // dodaj sva stanja koja mozes posjetiti prijelazom, ukljucujuci epsilone
            std::set<State> eps = get_eps(s->nxt[i]);
            nxt.insert(nxt.end(), eps.begin(), eps.end());
         }
      }
   }

   // promijeni trenutni skup stanja u sljedeci 
   curr.clear();
   curr.insert(nxt.begin(), nxt.end());

   // ako je neko stanje prihvatljivo vrati true, inace false
   return curr.count(all->finish);
}

