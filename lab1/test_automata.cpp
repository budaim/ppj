#include "automata.h"

#include <cstdio>
#include <iostream>
#include <string>

inline int run(Automata *a, std::string w) {
   int ret = 0;
   for (std::string::iterator it = w.begin(); it != w.end(); ++it) 
      ret = a->next(*it);
   return ret;
}

inline void test(Automata *a, std::string w, int sol) {
   int r = run(a, w);
   std::cout << w << " " << r << " " << sol;
   std::cout << ((r != sol) ? " ERROR" : "") << '\n';
   a->clear();
}

int main() {
   Automata *a = new Automata("kurac");
   //a->output();
   test(a, "kurac", 1);
   test(a, "kuraac", 0);
   test(a, "kuuracc", 0);
   test(a, "kura", 0);
   test(a, "kita", 0);
   test(a, "krac", 0);
   test(a, "kuracc", 0);

   Automata *b = new Automata("a*bc");
   //b->output();
   test(b, "bc", 1);
   test(b, "ab", 0);
   test(b, "abbc", 0);
   test(b, "abc", 1);
   test(b, "aaaabcc", 0);
   test(b, "aaaabc", 1);

   Automata *c = new Automata("acab|kurac");
   test(c, "accaab", 0);
   test(c, "kurac", 1);
   test(c, "acab", 1);
   test(c, "urac", 0);
   test(c, "bkurac", 0);

   Automata *d = new Automata("(ab|bc)*k");
   test(d, "abbck", 1);
   test(d, "bck", 1);
   test(d, "abk", 1);
   test(d, "k", 1);
   test(d, "abbcababbck", 1);
   test(d, "abababbcabk", 1);
   test(d, "bcbcabk", 1);
   test(d, "aabck", 0);
   test(d, "ak", 0);
   test(d, "abab", 0);
   test(d, "bc", 0);
   test(d, "abck", 0);

   Automata *e = new Automata("(a|b)*(c|d)");
   test(e,"c", 1);
   test(e,"d", 1);
   test(e,"cd", 0);
   test(e,"ac", 1);
   test(e,"bc", 1);
   test(e,"aabaabac", 1);
   test(e,"ababaab", 0);
   test(e,"ababad", 1);
   test(e,"ababacaad", 0);

   Automata *f = new Automata("(a|b)*(b|c)");
   test(f,"b", 1);
   test(f,"c", 1);
   test(f,"bb", 1);
   test(f,"ab", 1);
   test(f,"bc", 1);
   test(f,"abababaaab", 1);
   test(f,"ababababaac", 1);
   test(f,"abababaababa", 0);
   test(f,"aadsfabc", 0);

   return 0;
}


