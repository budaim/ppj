#ifndef UTILS_H
#define UTILS_H

#include <vector>

// definicija stanja
struct State_ {
   // let[i] je znak i-tog prijelaza iz stanja
   std::vector<int> let;
   // nxt[i] je stanje u koje vodi i-ti prijelaz
   std::vector<State_*> nxt;

   State_() {}

   void add_transition(int chr, State_* sta) {
      let.push_back(chr);
      nxt.push_back(sta);
   }
};

// definicija "komada" automata s pocetnim i konacnim stanjem
struct Part_ {
   State_ *start, *finish;

   Part_() {
      start = 0;
      finish = 0;
   }

   Part_(State_  *_start, State_ *_finish) {
      start = _start;
      finish = _finish;
   }
};


typedef Part_* Part;
typedef State_* State;

// definicije funkcija za spajanje komada
void kleene(Part);
void concat(Part, Part);
void makeor(Part, Part);

#endif


