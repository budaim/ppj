#include <cstdio>
#include <cstring>

#include "automata.h"

//#define LL_DEBUG
#include "log.h"

using std::vector;
using std::string;
using std::pair;
using std::make_pair;

#define MAXBUFF 100000

// Definicije jezika prema kojima se analizator ravna.  
// =============================================
// =============================================


/*
a[s] = lista (automat, pravilo) gdje automat prepoznaje
stringove koji odgovaraju regularnom izrazu pravila.
*/
vector< pair< Automata*, int > > a[nStates];


/*
Za svako pravilo izgradi inicijaliziraj automat i pohrani ga u listu
za odgovarajuce stanje.
*/
void initAutomata() {
  for (int i = 0; i < nRules; ++i) {
    a[fromState[i]].push_back(make_pair(new Automata(string(regex[i])), i));
  }
}

// stvari za ucitavanje

// pomocni buffer za fread
char buffer[MAXBUFF];

// nakon poziva readInput() sve je ucitano u input
string input = "";

void readInput() {
  while (fread(buffer, 1, MAXBUFF, stdin)) 
    input += string(buffer);
}

// kraj stvari za ucitavanje

// stanje analizatora 
int state = source;

// pozicija u inputu
int pos;

// brojac linija
int line;

// resetiraj sve automate za trenutno stanje
void clearAutomata() {
  for (int i = 0; i < a[state].size(); ++i)
    a[state][i].first->clear();
}

int maxPos;
int matchRule;
int alive;

/*
Obradi znak c i vrati true ako neki automat dodatkom
tog znaka prihvaca trenutni string.
U tom slucaju globalna varijabla matchRule sadrzi
indeks pravila za koje je automat prihvatio string.

alive kaze koliko automata jos moze prihvatiti. 
Sluzi kao optimizacija, jer u slucaju da nijedan automat ne 
moze prihvatiti ne moramo isprobavati daljnje znakove.
*/
bool process(char c) {
  bool ret = false;
  int newAlive = 0;
  // predaj znak c svim automatima za trenutno stanje (state)
  for (int i = a[state].size() - 1; i >= 0; --i) {
    if (a[state][i].first->next(c)) {
      // ovaj automat je prihvatio, pamtimo indeks pravila
      // koje taj automat prepoznaje
      matchRule = a[state][i].second;
      ret = true;
    }
    // provjeri moze li ovaj automat ikada u buducnosti, ako
    // moze onda racunaj da je "ziv"
    if (!a[state][i].first->lost())
      ++newAlive;
  }
  alive = newAlive;
  return ret;
}

// nadji najdulji prefiks od [pos:] koji valja
void maxMatch() {
  // najdalja pozicija do koje je neki automat prihvatio
  maxPos = -1;
  // u pocetku su svi automati "zivi"
  alive = a[state].size();
  // idi po inputu dok ima zivih automata i obradi znakove, 
  // pamti najdalju poziciju do koje je neki automat prihvatio
  for (int i = pos; i < input.size() && alive; ++i) 
    if (process(input[i])) 
      maxPos = i;
}

int main(void)
{
  initAutomata();
  readInput();

  // dok nije obradjen cijeli string...
  while (pos < input.size()) {
    // pronadji najdulji prefiks koji odgovara nekom pravilu
    maxMatch();
    if (maxPos == -1) {
      // nijedno pravilo se ne moze primijeniti, 
      // primijeni oporavak od pogreske preskakanjem trenutnog znaka
      ++pos;
    } else {
      // primijeni pravilo i ispisi ako ima ime
      int npos = 0;

      // ako ovo pravilo kaze da se moramo vratiti unatrag, onda se vratimo
      if (goBack[matchRule] != -1) 
        npos = pos + goBack[matchRule];
      // inace idemo od prvog sljedeceg znaka
      else
        npos = maxPos + 1;

      // ako ovo pravilo ima ime (odgovarajuci uniformni znak), ispisi ime, liniju i 
      // niz znakova koji je pronadjen
      if (uniforms[matchRule][0] != '-')
        printf("%s %d %s\n", uniforms[matchRule], line + 1, input.substr(pos, npos - pos).c_str());

      // ako je prepoznat novi redak, povecaj brojac redaka
      if (newLine[matchRule])
        ++line;
      
      // ako je potrebno promijeniti stanje zbog pravila koje primjenjujemo
      // promijenimo stanje kako pravilo kaze.
      if (toState[matchRule] != -1)
        state = toState[matchRule];

      // osvjezi trenutnu poziciju
      pos = npos;
    }
    
    // ocisti automate za sljedeci prolaz
    clearAutomata();
  }

  return 0;
}
