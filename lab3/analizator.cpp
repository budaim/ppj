#include <cstdio>
#include <cstring>
#include <cassert>

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

#define log(...) //fprintf(stderr, ##__VA_ARGS__)

vector< string > stree;

int alloc;
vector< vector< int > > T;

vector< string > split(string s) {
  string curr = "";
  vector< string > ret;

  bool stop = false;

  for (int i = 0; i < s.size(); ++i) {
    if (s[i] == ' ' && !stop) {
      if (curr.size()) ret.push_back(curr);
      curr = "";
    } 
    else {
      if (s[i] == '"')
        stop = true;
      curr += s[i];
    }
  }

  if (curr != "")
    ret.push_back(curr);

  while (ret.size() < 3)
    ret.push_back("$$FAKE$$");

  for (int i = ret.size() - 2; i >= 2; --i)
    ret[i] += ret[i + 1];

  return ret;
}

int getd(string s) {
  int d = 0;
  while (d < s.size() && s[d] == ' ') ++d;
  return d;
}

string node(int nid) {
  return split(stree[nid])[0];
}

string get_line(int nid) {
  return split(stree[nid])[1];
}

string get_text(int nid) {
  return split(stree[nid])[2];
}

int get_value(int nid) {
  string value = get_text(nid);
  long long v; 
  if (value.size() > 1 && value[1] == 'x')
    sscanf(value.c_str(), "%llx", &v);
  else if (value.size() > 1 && value[1] == '0')
    sscanf(value.c_str(), "%llo", &v);
  else
    sscanf(value.c_str(), "%lld", &v);
  return (int)v;
}

string get_BNF(int nid, int pos = -1) {
  string ret = node(nid) + " ::=";
  for (int i = 0; i < T[nid].size(); ++i) {
    int nnid = T[nid][i];
    ret += " " + node(nnid);
    if (pos != -1 && T[nnid].size() == 0)
      ret += "(" + get_line(nnid) + "," + get_text(nnid) + ")";
  }
  return ret;
}

string unarray(string type) {
  if (type[0] != 'A') return type;
  return type.substr(1, type.size() - 1);
}

string unconst(string type) {
  if (type[0] != 'C') return type;
  return type.substr(1, type.size() - 1);
}

bool is_array(string type) {
  return type[0] == 'A';
}

bool is_const(string type) {
  return type[0] == 'C';
}

bool compat(string a, string b) {
  if (a == b) 
    return true;
  if (a == "C" + b)
    return true;
  if ("C" + a == b)
    return true;
  if (unconst(a) == "int" && unconst(b) == "char")
    return true;
  if (is_array(b) && !is_const(unarray(b)) && a == "AC" + unarray(b))
    return true;
  return false;
}

struct info {
  vector< string > types;
  vector< string > names;
  bool is_function;
  bool is_loop;
  bool is_boundary;
  bool is_char_array;
  bool is_array;
  bool exists;
  string ret_type;
  int lval;
  int nelem;

  info() { 
    exists = false; 
    is_boundary = false; 
    is_function = is_loop = false; 
    is_char_array = false;
    lval = 0; 
  }
  info(bool b) { 
    exists = true; 
    is_boundary = b; 
    is_function = is_loop = false; 
    is_char_array = false;
    lval = 0; 
  }

  void add_type(string type) {
    log("pusham %s\n", type.c_str());
    log("%d\n", types.size());
    types.push_back(type);
  }

  void add_lval(int lvalue) {
    lval = lvalue;
  }

  void add_nelem(int n) {
    nelem = n;
  }

  void add_name(string name) {
    names.push_back(name);
  }
};

struct symbol_table {
  int num_alloc;
  vector< info > stk;

  symbol_table() { num_alloc = 0; }

  void open_context(string type = "") {
    log("otvaram context %s\n", type.c_str());
    info i(1);
    i.ret_type = type;
    if (type == "loop") {
      i.is_loop = 1;
    }
    else if (type.size()) {
      i.is_function = 1;
    }
    stk.push_back(i);
  }

  void close_context() {
    info e;
    do {
      e = stk.back(); 
      stk.pop_back();
    } while (!e.is_boundary);
  }

  int add_identifier(info id) {
    log("<<<<<<< dodajem identifier %s\n", id.names[0].c_str());
    stk.push_back(id);
  }

  bool check_in_function(string type) {
    int i;
    for (i = stk.size() - 1; i >= 0 && !(stk[i].is_function && stk[i].is_boundary); --i);
    if (i < 0) return false;
    return stk[i].is_function && compat(stk[i].ret_type, type);
  }

  bool check_in_loop() {
    int i;
    for (i = stk.size() - 1; i >= 0 && !(stk[i].is_loop && stk[i].is_boundary); --i);
    if (i < 0) return false;
    return stk[i].is_loop;
  }

  info layered_identifier_lookup(string name) {
    for (int i = stk.size() - 1; i >= 0; --i)
      if (stk[i].names.size() && stk[i].names[0] == name)
        return stk[i];
    return info();
  }

  info local_identifier_lookup(string name) {
    for (int i = stk.size() - 1; i >= 0 && !(stk[i].is_boundary); --i) {
      if (stk[i].names.size() && stk[i].names[0] == name)
        return stk[i];
    }
    log("nista lookup\n");
    return info();
  }
  
  info strict_local_identifier_lookup(string name) {
    for (int i = stk.size() - 1; i >= 0 && !stk[i].is_boundary; --i) {
      if (stk[i].names.size() && stk[i].names[0] == name)
        return stk[i];
    }
    return info();
  }

  info global_identifier_lookup(string name) {
    for (int i = 0; i < stk.size() && !stk[i].is_boundary; ++i)
      if (stk[i].names[0] == name)
        return stk[i];
    return info();
  }
};

symbol_table st;

info explore(int nid, string ntip = "", bool ctx = 1);


// TODO
string get_type(string name) {
  info i = st.layered_identifier_lookup(name);
  log("get type %s: %d %d\n", name.c_str(), i.types.size(), i.exists);
  return i.types[0];
}

// TODO
int get_lval(string name) {
  info i = st.layered_identifier_lookup(name);
  return i.lval;
}

bool reduces_to(int nid, string end) {
  for (;;) {
    if (T[nid].size() > 1) 
      return false;

    if (T[nid].size() == 0)
      return node(nid) == end;

    nid = T[nid][0];
  }

  return false;
}

void assign_type(info &a, info b) {
  a.is_function = b.is_function;
  a.types = b.types;
  a.ret_type = b.ret_type;
}

int char_sequence_length(int nid) {
  while (T[nid].size()) 
    nid = T[nid][0];
  bool escaped = false;
  int ret = 0;
  string t = get_text(nid);
  for (int i = 1; i < t.size() - 1; ++i) {
    if (t[i] == '\\') {
      if (!(escaped = !escaped))
        ++ret;
    }
    else {
      ++ret;
      escaped = false;
    }
  }
  return ret;
}

info get_global_declaration(string name) {
  return st.global_identifier_lookup(name);
}

info get_local_declaration(string name) {
  return st.local_identifier_lookup(name);
}

vector< string > declared;
vector< string > defined;
vector< vector< string > > decTi, defTi;
vector< string > decTo, defTo;

// za funkciju
void add_function_declaration(string name, vector< string > ti, string to) {
  declared.push_back(name);
  decTi.push_back(ti);
  decTo.push_back(to);
  info i(0);
  i.is_function = true;
  i.add_name(name);
  i.types = ti;
  i.ret_type = to;
  st.add_identifier(i);

  log("dodajem funkciju %s\n", name.c_str());

  for (int i = 0; i < ti.size(); ++i)
    log("%s\n", ti[i].c_str());
}

// za funkciju
void add_function_definition(string name, vector< string > ti, string to) {
  defined.push_back(name);
  defTi.push_back(ti);
  defTo.push_back(to);
  
  log("dodajem definiciju funkcije %s\n", name.c_str());

  for (int i = 0; i < ti.size(); ++i)
    log("%s\n", ti[i].c_str());
}

void add_declaration(string name, string type) {
  info i(0);
  i.is_function = false;
  i.add_name(name);
  i.add_type(type);
  i.add_lval(!is_const(type));
  st.add_identifier(i);
}


void fail(int nid, int pos = 0) {
  printf("%s\n", get_BNF(nid, pos).c_str());
  exit(0);
}

void check_declared(int nid, string name) {
  info i = st.layered_identifier_lookup(name);
  if (!i.exists) 
    fail(nid);
}

void check_int_range(int nid, string value) {
  while (value.size() >= 2 && value[0] == '0' && value[1] == '0')
    value = value.substr(1, value.size() - 1);
  if (value.size() > 15)
    fail(nid);
  long long v; 
  if (value.size() > 1 && value[1] == 'x')
    sscanf(value.c_str(), "%llx", &v);
  else if (value.size() > 1 && value[1] == '0')
    sscanf(value.c_str(), "%llo", &v);
  else
    sscanf(value.c_str(), "%lld", &v);
  if (v < -2147483648 || v > 2147483647)
    fail(nid);
}

void check_lval(int nid, int pos, int lval, int val) {
  if (lval != val)
    fail(nid, pos);
}

void check_compat(int nid, int pos, string a, string b) {
  log("compat %s %s = %d\n", a.c_str(), b.c_str(), compat(a, b));
  if (!compat(a, b))
    fail(nid, pos);
}

void check_compats(int nid, int pos, vector< string > a, vector< string > b) {
  log("%d %d\n", a.size(), b.size());
  if (a.size() != b.size())
    fail(nid, pos);
  
  for (int i = 0; i < a.size(); ++i) {
    check_compat(nid, pos, a[i], b[i]);
    log("compat %s %s\n", a[i].c_str(), b[i].c_str());
  }
}

void check_all_compat(int nid, int pos, vector< string > types, string t) {
  for (int i = 0; i < types.size(); ++i)
    check_compat(nid, pos, t, types[i]);
}

void check_character(int nid, string c) {
  if (c == "'\\'")
    fail(nid);
  if (c.size() > 3)
    if (c != "'\\t'")
      if (c != "'\\n'")
        if (c != "'\\0'")
          if (c != "'\\\\'")
            if (c != "'\\\"'")
              if (c != "'\\''")
              fail(nid);
}

void check_characters(int nid, string s) {
  log("check niz znakova %s\n", s.c_str());
  bool escaped = false;
  for (int i = 1; i < s.size() - 1; ++i) {
    if (s[i] == '\\') {
      escaped = !escaped;
    }
    else if (escaped) {
      if (s[i] != 't')
        if (s[i] != 'n')
          if (s[i] != '0')
            if (s[i] != '\'')
              if (s[i] != '"')
                fail(nid);
      escaped = false;
    }
    else if (s[i] == '"')
      fail(nid);
  }
  if (escaped)
    fail(nid);
}

void check_array(int nid, string type) {
  if (type[0] != 'A')
    fail(nid);
}

void check_is_function(int nid, info i) {
  if (!i.is_function)
    fail(nid);
}

void check_type(int nid, string a, string b) {
  if (a != b)
    fail(nid);
}

void check_castable(int nid, string a, string b) {
  log("gledam %s %s\n", a.c_str(), b.c_str());
  if (!compat(b, a)) {
    log("dobio tipove %s %s\n", a.c_str(), b.c_str());
    if (unconst(a) != "int" && unconst(a) != "char")
      fail(nid);
    if (unconst(b) != "int" && unconst(b) != "char")
      fail(nid);
  }
}

void check_non_void(int nid, string type) {
  if (type == "void")
    fail(nid);
}

void check_in_loop(int nid) {
  if (!st.check_in_loop())
    fail(nid);
}

void check_in_function(int nid, string type) {
  if (!st.check_in_function(type)) {
    log("nismo u funkciji tipa %s\n", type.c_str());
    fail(nid);
  }
}

void check_not_const(int nid, string type) {
  if (type[0] == 'C')
    fail(nid);
}

void check_non_function(int nid, info i) {
  if (i.is_function) {
    log("dobio funkciju :O %d\n", nid);
    fail(nid);
  }
}

void check_function(int nid, info i, vector< string > ti, string to) {
  if (i.types.size() != ti.size())
    fail(nid);
  
  for (int j = 0; j < ti.size(); ++j)
    if (ti[j] != i.types[j])
      fail(nid);

  if (i.ret_type != to)
    fail(nid);
}

void check_not_in(int nid, vector< string > v, string s) {
  for (int i = 0; i < v.size(); ++i)
    if (v[i] == s)
      fail(nid);
}

void check_non_const(int nid, string type) {
  if (type[0] == 'C') {
    log("tip %s je const\n", type.c_str());
    fail(nid);
  }
}

void check_non_array(int nid, string type) {
  if (type[0] == 'A') {
    log("tip %s je array\n", type.c_str());
    fail(nid);
  }
}

void check_non_const_array(int nid, string type) {
  if (type[0] == 'A' && type[1] == 'C') {
    log("tip %s\n je const array", type.c_str());
    fail(nid);
  }
}

void check_lte(int nid, int pos, int a, int b) {
  log("lte check %d %d\n", a, b);
  if (a > b)
    fail(nid, pos);
}

void check_undefined(int nid, string name) {
  for (int i = 0; i < defined.size(); ++i)
    if (defined[i] == name)
      fail(nid);
}

void check_local_undefined(int nid, string name) {
  info i0 = st.strict_local_identifier_lookup(name);
  log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!local undefined %s: %d\n", name.c_str(), i0.exists);
  if (i0.exists) 
    fail(nid);
}

void check_index(int nid, string value) {
  if (value.size() > 15) 
    fail(nid);
  long long v;
  if (value.size() > 1 && value[1] == 'x')
    sscanf(value.c_str(), "%llx", &v);
  else if (value.size() > 1 && value[0] == '0')
    sscanf(value.c_str(), "%llo", &v);
  else
    sscanf(value.c_str(), "%lld", &v);
  log("vrijednost %lld\n", v);
  if (v < 1 || v > 1024)
    fail(nid);
}

void identity(info &ret, int nid, int pos = 0) {
  ret = explore(T[nid][pos]);
}

void inc_dec(info &ret, int nid, int pos = 0) {
  info i0 = explore(T[nid][pos]);
  check_lval(nid, pos, i0.lval, 1);
  check_compat(nid, pos, "int", i0.types[0]);
  ret.add_type("int");
  ret.add_lval(0);
}

void operation(info &ret, int nid)  {
  info i0 = explore(T[nid][0]);
  check_compat(nid, 1, "int", i0.types[0]);
  info i1 = explore(T[nid][2]);
  check_compat(nid, 1, "int", i1.types[0]);
  ret.add_type("int");
  ret.add_lval(0);
}

info explore(int nid, string ntip, bool ctx) {
  string bnf = get_BNF(nid);
  info ret;

  log("...%s\n", get_BNF(nid, 0).c_str());
  assert(T[nid].size() > 0);
  /*

  for (int i = 0; i < T[nid].size(); ++i)
    explore(T[nid][i]);
    */


  // izrazi
  if (bnf == "<primarni_izraz> ::= IDN") {
    string name = get_text(T[nid][0]);
    check_declared(nid, name);
    info i0 = st.layered_identifier_lookup(name);
    log("%d\n", i0.types.size());
    log("pozivam se na identifikator %s, lval je %d\n", name.c_str(), i0.lval);
    ret = i0;
  }
  else if (bnf == "<primarni_izraz> ::= BROJ") {
    check_int_range(nid, get_text(T[nid][0]));
    ret.add_type("int");
    ret.add_lval(0);
  }
  else if (bnf == "<primarni_izraz> ::= ZNAK") {
    check_character(nid, get_text(T[nid][0]));
    ret.add_type("char");
    ret.add_lval(0);
  }
  else if (bnf == "<primarni_izraz> ::= NIZ_ZNAKOVA") {
    check_characters(nid, get_text(T[nid][0]));
    ret.add_type("ACchar");
    ret.add_lval(0);
  }
  else if (bnf == "<primarni_izraz> ::= L_ZAGRADA <izraz> D_ZAGRADA") {
    identity(ret, nid, 1);
  }
  else if (bnf == "<postfiks_izraz> ::= <primarni_izraz>") {
    identity(ret, nid);
    log("%d\n", ret.types.size());
  }
  else if (bnf == "<postfiks_izraz> ::= <postfiks_izraz> L_UGL_ZAGRADA <izraz> D_UGL_ZAGRADA") {
    info i0 = explore(T[nid][0]);
    log("check array %s\n", i0.types[0].c_str());
    check_array(nid, i0.types[0]);
    info i1 = explore(T[nid][2]);
    log("check broj\n");
    check_compat(nid, 1, "int", i1.types[0]);
    ret.add_type(unarray(i0.types[0]));
    ret.add_lval(unarray(i0.types[0])[0] != 'C');
  }
  else if (bnf == "<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA D_ZAGRADA") {
    info i0 = explore(T[nid][0]);
    check_is_function(nid, i0);
    check_type(nid, i0.types[0], "void");
    ret.add_type(i0.ret_type);
    ret.add_lval(0);
  }
  else if (bnf == "<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA <lista_argumenata> D_ZAGRADA") {
    info i0 = explore(T[nid][0]);
    info i1 = explore(T[nid][2]);
    log("i0 %d tipova\n", i0.types.size());
    log("i1 %d tipova\n", i1.types.size());
    check_is_function(nid, i0);
    check_compats(nid, 1, i0.types, i1.types);
    ret.add_type(i0.ret_type);
    ret.add_lval(0);
  }
  else if (bnf == "<postfiks_izraz> ::= <postfiks_izraz> OP_INC" ||
     bnf == "<postfiks_izraz> ::= <postfiks_izraz> OP_DEC") {
    inc_dec(ret, nid);
  }
  else if (bnf == "<lista_argumenata> ::= <izraz_pridruzivanja>") {
    info i0 = explore(T[nid][0]);
    //ret.add_type(i0.types[0]);
    assign_type(ret, i0);
  }
  else if (bnf == "<lista_argumenata> ::= <lista_argumenata> ZAREZ <izraz_pridruzivanja>") {
    info i0 = explore(T[nid][0]);
    info i1 = explore(T[nid][2]);
    ret.types = i0.types;
    ret.add_type(i1.types[0]);
  }
  else if (bnf == "<unarni_izraz> ::= <postfiks_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<unarni_izraz> ::= OP_INC <unarni_izraz>" ||
     bnf == "<unarni_izraz> ::= OP_DEC <unarni_izraz>") {
    inc_dec(ret, nid, 1);
  }
  else if (bnf == "<unarni_izraz> ::= <unarni_operator> <cast_izraz>") {
    info i0 = explore(T[nid][1]);
    check_compat(nid, 0, "int", i0.types[0]);
    ret.add_type("int");
    ret.add_lval(0);
  }
  else if (node(nid) == "<unarni_operator>") {
  }
  else if (bnf == "<cast_izraz> ::= <unarni_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<cast_izraz> ::= L_ZAGRADA <ime_tipa> D_ZAGRADA <cast_izraz>") {
    info i0 = explore(T[nid][1]);
    info i1 = explore(T[nid][3]);
    check_non_function(nid, i1);
    check_castable(nid, i1.types[0], i0.types[0]);
    //ret.add_type(i0.types[0]);
    assign_type(ret, i0);
    ret.add_lval(0);
  }
  else if (bnf == "<ime_tipa> ::= <specifikator_tipa>") {
    identity(ret, nid);
    log("ime tipa = %s\n", ret.types[0].c_str());
  }
  else if (bnf == "<ime_tipa> ::= KR_CONST <specifikator_tipa>") {
    info i0 = explore(T[nid][1]);
    check_non_void(nid, i0.types[0]);
    ret.add_type("C" + i0.types[0]);
  }
  else if (bnf == "<specifikator_tipa> ::= KR_VOID") {
    log("imam void!!!\n");
    ret.add_type("void");
  }
  else if (bnf == "<specifikator_tipa> ::= KR_CHAR") {
    ret.add_type("char");
  }
  else if (bnf == "<specifikator_tipa> ::= KR_INT") {
    ret.add_type("int");
  }
  else if (bnf == "<multiplikativni_izraz> ::= <cast_izraz>") {
    identity(ret, nid);
    log("multiplikativni s %d\n", ret.types.size());
  }
  else if (bnf == "<multiplikativni_izraz> ::= <multiplikativni_izraz> OP_PUTA <cast_izraz>" ||
      bnf ==  "<multiplikativni_izraz> ::= <multiplikativni_izraz> OP_DIJELI <cast_izraz>" || 
      bnf ==  "<multiplikativni_izraz> ::= <multiplikativni_izraz> OP_MOD <cast_izraz>") {
    log("imam taj OP_MOD izraz\n");
    operation(ret, nid);
  }
  else if (bnf == "<aditivni_izraz> ::= <multiplikativni_izraz>") {
    identity(ret, nid);
    log(".aditivni izraz s %d tipova\n", ret.types.size());
  }
  else if (bnf == "<aditivni_izraz> ::= <aditivni_izraz> PLUS <multiplikativni_izraz>" ||
      bnf == "<aditivni_izraz> ::= <aditivni_izraz> MINUS <multiplikativni_izraz>") {
    operation(ret, nid);
    log("aditivni izraz s %d tipova\n", ret.types.size());
  }
  else if (bnf == "<odnosni_izraz> ::= <aditivni_izraz>") {
    identity(ret, nid);
    log("odnosni izraz s %d tipova\n", ret.types.size());
  }
  else if (bnf == "<odnosni_izraz> ::= <odnosni_izraz> OP_LT <aditivni_izraz>" ||
      bnf == "<odnosni_izraz> ::= <odnosni_izraz> OP_GT <aditivni_izraz>" ||
      bnf == "<odnosni_izraz> ::= <odnosni_izraz> OP_LTE <aditivni_izraz>" ||
      bnf == "<odnosni_izraz> ::= <odnosni_izraz> OP_GTE <aditivni_izraz>") {
    operation(ret, nid);
  }
  else if (bnf == "<jednakosni_izraz> ::= <odnosni_izraz>") {
    identity(ret, nid);
    log("jednakosni izraz s %d\n", ret.types.size());
  }
  else if (bnf == "<jednakosni_izraz> ::= <jednakosni_izraz> OP_EQ <odnosni_izraz>" ||
      bnf == "<jednakosni_izraz> ::= <jednakosni_izraz> OP_NEQ <odnosni_izraz>") {
    operation(ret, nid);
  }
  else if (bnf == "<bin_i_izraz> ::= <jednakosni_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<bin_i_izraz> ::= <bin_i_izraz> OP_BIN_I <jednakosni_izraz>") {
    operation(ret, nid);
  }
  else if (bnf == "<bin_xili_izraz> ::= <bin_i_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<bin_xili_izraz> ::= <bin_xili_izraz> OP_BIN_XILI <bin_i_izraz>") {
    operation(ret, nid);
  }
  else if (bnf == "<bin_ili_izraz> ::= <bin_xili_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<bin_ili_izraz> ::= <bin_ili_izraz> OP_BIN_ILI <bin_xili_izraz>") {  
    operation(ret, nid);
  }
  else if (bnf == "<log_i_izraz> ::= <bin_ili_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<log_i_izraz> ::= <log_i_izraz> OP_I <bin_ili_izraz>") {
    operation(ret, nid);
  }
  else if (bnf == "<log_ili_izraz> ::= <log_i_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<log_ili_izraz> ::= <log_ili_izraz> OP_ILI <log_i_izraz>") {
    operation(ret, nid);
  }
  else if (bnf == "<izraz_pridruzivanja> ::= <log_ili_izraz>") {
    identity(ret, nid);
  }
  else if (bnf == "<izraz_pridruzivanja> ::= <postfiks_izraz> OP_PRIDRUZI <izraz_pridruzivanja>") {
    info i0 = explore(T[nid][0]);
    log("ide lval za PRIDRUZI %d\n", i0.lval);
    check_lval(nid, 0, i0.lval, 1);
    info i1 = explore(T[nid][2]);
    log("tipovi u pridruzivanju %s %s\n", i1.types[0].c_str(), i0.types[0].c_str());
    if (is_array(i0.types[0]) && is_array(i1.types[0]))
        fail(nid);
    check_compat(nid, 1, i0.types[0], i1.types[0]);
    //ret.add_type(i0.types[0]);
    assign_type(ret, i0);
    ret.add_lval(0);
  }
  else if (bnf == "<izraz> ::= <izraz_pridruzivanja>") {
    identity(ret, nid);
    log("?%d\n", ret.types.size());
  }
  else if (bnf == "<izraz> ::= <izraz> ZAREZ <izraz_pridruzivanja>") {
    info i0 = explore(T[nid][0]);
    info i1 = explore(T[nid][2]);
    //ret.add_type(i1.types[0]);
    assign_type(ret, i1);
    ret.add_lval(0);
  }
  // naredbe
  else if (bnf == "<slozena_naredba> ::= L_VIT_ZAGRADA <lista_naredbi> D_VIT_ZAGRADA") {
    if (ctx && ntip != "loop")
      st.open_context(ntip);
    explore(T[nid][1]);
    if (ctx && ntip != "loop")
      st.close_context();
  }
  else if (bnf == "<slozena_naredba> ::= L_VIT_ZAGRADA <lista_deklaracija> <lista_naredbi> D_VIT_ZAGRADA") {
    if (ctx && ntip != "loop")
      st.open_context(ntip);
    explore(T[nid][1]);
    explore(T[nid][2]);
    if (ctx && ntip != "loop")
      st.close_context();
  }
  else if (bnf == "<lista_naredbi> ::= <naredba>") {
    explore(T[nid][0]);
  }
  else if (bnf == "<lista_naredbi> ::= <lista_naredbi> <naredba>") {
    explore(T[nid][0]);
    explore(T[nid][1]);
  }
  else if (node(nid) == "<naredba>") {
    explore(T[nid][0], ntip);
  }
  else if (bnf == "<izraz_naredba> ::= TOCKAZAREZ") {
    ret.add_type("int");
  }
  else if (bnf == "<izraz_naredba> ::= <izraz> TOCKAZAREZ") {
    info i0 = explore(T[nid][0]);
    log("%d\n", i0.types.size());
    //ret.add_type(i0.types[0]);
    assign_type(ret, i0);
  }
  else if (bnf == "<naredba_grananja> ::= KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba>") {
    info i0 = explore(T[nid][2]);
    check_non_function(nid, i0);
    log("izraz s tipova %d\n", i0.types.size());
    check_compat(nid, 0, "int", i0.types[0]);
    explore(T[nid][4]);
  }
  else if (bnf == "<naredba_grananja> ::= KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba> KR_ELSE <naredba>") {
    info i0 = explore(T[nid][2]);
    check_compat(nid, 0, "int", i0.types[0]);
    explore(T[nid][4]);
    explore(T[nid][6]);
  }
  else if (bnf == "<naredba_petlje> ::= KR_WHILE L_ZAGRADA <izraz> D_ZAGRADA <naredba>") {
    info i0 = explore(T[nid][2]);
    check_compat(nid, 0, "int", i0.types[0]);
    st.open_context("loop");
    explore(T[nid][4], "loop");
    st.close_context();
  }
  else if (bnf == "<naredba_petlje> ::= KR_FOR L_ZAGRADA <izraz_naredba> <izraz_naredba> D_ZAGRADA <naredba>") {
    explore(T[nid][2]);
    info i0 = explore(T[nid][3]);
    check_compat(nid, 0, "int", i0.types[0]);
    st.open_context("loop");
    explore(T[nid][5], "loop");
    st.close_context();
  }
  else if (bnf == "<naredba_petlje> ::= KR_FOR L_ZAGRADA <izraz_naredba> <izraz_naredba> <izraz> D_ZAGRADA <naredba>") {
    explore(T[nid][2]);
    info i0 = explore(T[nid][3]);
    check_compat(nid, 0, "int", i0.types[0]);
    explore(T[nid][4]);
    st.open_context("loop");
    explore(T[nid][6], "loop");
    st.close_context();
  }
  else if (bnf == "<naredba_skoka> ::= KR_CONTINUE TOCKAZAREZ" ||
      bnf == "<naredba_skoka> ::= KR_BREAK TOCKAZAREZ") {
    check_in_loop(nid);
  }
  else if (bnf == "<naredba_skoka> ::= KR_RETURN TOCKAZAREZ") {
    check_in_function(nid, "void");
  }
  else if (bnf == "<naredba_skoka> ::= KR_RETURN <izraz> TOCKAZAREZ") {
    info i0 = explore(T[nid][1]);
    check_non_function(nid, i0);
    check_in_function(nid, i0.types[0]);
  }
  else if (bnf == "<prijevodna_jedinica> ::= <vanjska_deklaracija>") {
    explore(T[nid][0]);
  }
  else if (bnf == "<prijevodna_jedinica> ::= <prijevodna_jedinica> <vanjska_deklaracija>") {
    explore(T[nid][0]);
    explore(T[nid][1]);
  }
  else if (node(nid) == "<vanjska_deklaracija>") {
    explore(T[nid][0]);
  }
  else if (bnf == "<definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA KR_VOID D_ZAGRADA <slozena_naredba>") {
    info i0 = explore(T[nid][0]);
    check_not_const(nid, i0.types[0]);
    string name = get_text(T[nid][1]);
    check_undefined(nid, name);
    info i1 = get_global_declaration(name);

    vector< string > argtypes(1, "void");

    if (i1.exists) {
      check_function(nid, i1, argtypes, i0.types[0]);
    }

    add_function_declaration(name, argtypes, i0.types[0]);
    add_function_definition(name, argtypes, i0.types[0]);

    explore(T[nid][5], i0.types[0]);
  }
  else if (bnf == "<definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA <lista_parametara> D_ZAGRADA <slozena_naredba>") {
    info i0 = explore(T[nid][0]);
    check_not_const(nid, i0.types[0]);
    string name = get_text(T[nid][1]);
    check_undefined(nid, name);

    info i1 = explore(T[nid][3]);
    info i2 = get_global_declaration(name);

    if (i2.exists) {
      check_function(nid, i2, i1.types, i0.types[0]);
    }

    log("ide definicija funkcije %s...\n", name.c_str());

    for (int i = 0; i < i1.types.size(); ++i)
      log("tip %s\n", i1.types[i].c_str());

    add_function_declaration(name, i1.types, i0.types[0]);
    add_function_definition(name, i1.types, i0.types[0]);

    st.open_context(i0.types[0]);

    for (int i = 0; i < i1.names.size(); ++i)
      add_declaration(i1.names[i], i1.types[i]);

    explore(T[nid][5], i0.types[0], 0);

    st.close_context();
  }
  else if (bnf == "<lista_parametara> ::= <deklaracija_parametra>") {
    info i0 = explore(T[nid][0]);
    //ret.add_type(i0.types[0]);
    assign_type(ret, i0);
    ret.add_name(i0.names[0]);
  }
  else if (bnf == "<lista_parametara> ::= <lista_parametara> ZAREZ <deklaracija_parametra>") {
    info i0 = explore(T[nid][0]);
    info i1 = explore(T[nid][2]);
    check_not_in(nid, i0.names, i1.names[0]);
    ret.types = i0.types;
    ret.add_type(i1.types[0]);
    ret.names = i0.names;
    ret.add_name(i1.names[0]);
  }
  else if (bnf == "<deklaracija_parametra> ::= <ime_tipa> IDN") {
    info i0 = explore(T[nid][0]);
    check_non_void(nid, i0.types[0]);
    //ret.add_type(i0.types[0]);
    assign_type(ret, i0);
    ret.add_name(get_text(T[nid][1]));
  }
  else if (bnf == "<deklaracija_parametra> ::= <ime_tipa> IDN L_UGL_ZAGRADA D_UGL_ZAGRADA") {
    info i0 = explore(T[nid][0]);
    check_non_void(nid, i0.types[0]);
    ret.add_type("A" + i0.types[0]);
    ret.add_name(get_text(T[nid][1]));
  }
  else if (bnf == "<lista_deklaracija> ::= <deklaracija>") {
    explore(T[nid][0]);
  }
  else if (bnf == "<lista_deklaracija> ::= <lista_deklaracija> <deklaracija>") {
    explore(T[nid][0]);
    explore(T[nid][1]);
  }
  else if (bnf == "<deklaracija> ::= <ime_tipa> <lista_init_deklaratora> TOCKAZAREZ") {
    info i0 = explore(T[nid][0]);
    explore(T[nid][1], i0.types[0]);
  }
  else if (bnf == "<lista_init_deklaratora> ::= <init_deklarator>") {
    explore(T[nid][0], ntip);
  }
  else if (bnf == "<lista_init_deklaratora> ::= <lista_init_deklaratora> ZAREZ <init_deklarator>") {
    explore(T[nid][0], ntip);
    explore(T[nid][2], ntip);
  }
  else if (bnf == "<init_deklarator> ::= <izravni_deklarator>") {
    info i0 = explore(T[nid][0], ntip);
    log(".. %d\n", (int)i0.types.size());
    if (!i0.is_function) {
      check_non_const(nid, i0.types[0]);
      check_non_const_array(nid, i0.types[0]);
    }
  }
  else if (bnf == "<init_deklarator> ::= <izravni_deklarator> OP_PRIDRUZI <inicijalizator>") {
    info i0 = explore(T[nid][0], ntip);
    info i1 = explore(T[nid][2], ntip);

    log("izravni deklarator je %s\n", i0.types[0].c_str());
    log("inicijalizator tipovi:\n");

    for (int i = 0; i < i1.types.size(); ++i)
      log("%s\n", i1.types[i].c_str());

    if (!is_array(i0.types[0])) {
      if (i1.is_char_array || i1.is_array)
        fail(nid);
      check_non_function(nid, i1);
      if (i1.types.size())
        check_compat(nid, 1, i0.types[0], unconst(i1.types[0]));
    }
    else {
      if (i1.types.size() && !is_array(i1.types[0]) && !i1.is_array)
        fail(nid);
      log("provjera lte:\n");
      if (reduces_to(T[nid][2], "NIZ_ZNAKOVA"))
        check_lte(nid, 1, i1.nelem + 1, i0.nelem);
      else
        check_lte(nid, 1, i1.nelem, i0.nelem);
      log("provjera all_compat:\n");
      check_all_compat(nid, 1, i1.types, unconst(unarray(i0.types[0])));
    }
  }
  else if (bnf == "<izravni_deklarator> ::= IDN") {
    check_non_void(nid, ntip);
    string name = get_text(T[nid][0]);
    check_local_undefined(nid, name);
    add_declaration(name, ntip);
    ret.add_type(ntip);
  }
  else if (bnf == "<izravni_deklarator> ::= IDN L_UGL_ZAGRADA BROJ D_UGL_ZAGRADA") {
    check_non_void(nid, ntip);
    string name = get_text(T[nid][0]);
    check_local_undefined(nid, name);
    check_index(nid, get_text(T[nid][2]));
    add_declaration(name, "A" + ntip);
    log("radim deklaraciju niza A%s\n", ntip.c_str());
    ret.add_type("A" + ntip);
    ret.add_nelem(get_value(T[nid][2]));
  }
  else if (bnf == "<izravni_deklarator> ::= IDN L_ZAGRADA KR_VOID D_ZAGRADA") {
    string name = get_text(T[nid][0]);
    info i0 = get_local_declaration(name);

    vector< string > argtypes(1, "void");

    if (i0.exists) {
      check_function(nid, i0, argtypes, ntip);
    }
    else {
      add_function_declaration(name, argtypes, ntip);
    }

    ret.is_function = true;
    ret.ret_type = ntip;
  }
  else if (bnf == "<izravni_deklarator> ::= IDN L_ZAGRADA <lista_parametara> D_ZAGRADA") {
    info i0 = explore(T[nid][2]);
    string name = get_text(T[nid][0]);
    info i1 = get_local_declaration(name);

    log("exists %s? : %d\n", name.c_str(), i1.exists);

    for (int i = 0; i < st.stk.size(); ++i)
      if (st.stk[i].names.size())
        log(":: %s : %d\n", st.stk[i].names[0].c_str(), st.stk[i].is_function);

    if (i1.exists) {
      log("imam funkciju %s\n", name.c_str());
      log("in:\n");
      for (int i = 0; i < i1.types.size(); ++i)
        log("%s\n", i1.types[i].c_str());
      log("out: %s\n", i1.ret_type.c_str());
      check_function(nid, i1, i0.types, ntip);
    }
    else {
      add_function_declaration(name, i0.types, ntip);
      log("ide deklaracija funkcije %s...\n", name.c_str());

      for (int i = 0; i < i0.types.size(); ++i)
        log("tip %s\n", i0.types[i].c_str());
    }

    ret.is_function = true;
    ret.types = i0.types;
    ret.ret_type = ntip;
  }
  else if (bnf == "<inicijalizator> ::= <izraz_pridruzivanja>") {
    if (reduces_to(nid, "NIZ_ZNAKOVA")) {
      explore(T[nid][0]);
      int nelem = char_sequence_length(nid);
      ret.add_nelem(nelem);
      ret.is_char_array = true;
      ret.is_array = true;
      for (int i = 0; i < nelem; ++i)
        ret.add_type("char");
    }
    else {
      info i0 = explore(T[nid][0]);
      assign_type(ret, i0);
    }
  }
  else if (bnf == "<inicijalizator> ::= L_VIT_ZAGRADA <lista_izraza_pridruzivanja> D_VIT_ZAGRADA") {
    info i0 = explore(T[nid][1]);
    ret.add_nelem(i0.nelem);
    ret.types = i0.types;
    ret.is_array = true;
  }
  else if (bnf == "<lista_izraza_pridruzivanja> ::= <izraz_pridruzivanja>") {
    info i0 = explore(T[nid][0]);
    assign_type(ret, i0);
    ret.add_nelem(1);
  }
  else if (bnf == "<lista_izraza_pridruzivanja> ::= <lista_izraza_pridruzivanja> ZAREZ <izraz_pridruzivanja>") {
    info i0 = explore(T[nid][0]);
    info i1 = explore(T[nid][2]);
    ret.types = i0.types;
    ret.add_type(i1.types[0]);
    ret.add_nelem(i0.nelem + 1);
  }

  return ret;
}

// restrukturira sintaksno stablo u ljepsi oblik
int restructure() {
  int now = alloc++;

  while (alloc < T.size() && getd(stree[alloc]) == getd(stree[now]) + 1) 
    T[now].push_back(restructure());

  return now;
}

int main(void)
{
  string line;
  while (getline(cin, line)) 
    stree.push_back(line);

  T.resize(stree.size());
  restructure();
  explore(0);

  bool got_main = 0;

  for (int i = 0; i < defined.size(); ++i) {
    if (defined[i] != "main") continue;
    got_main = true;
    if (defTi[i].size() != 1 || defTi[i][0] != "void" || defTo[i] != "int") {
      puts("main");
      exit(0);
    }
  }

  if (!got_main) {
    puts("main");
    exit(0);
  }

  //sort(defined.begin(), defined.end());

  for (int i = 0; i < declared.size(); ++i) {
    bool ok = false;
    for (int j = 0; j < defined.size(); ++j) {
      if (declared[i] != defined[j])
        continue;
      if (decTi[i] != defTi[j]) {
        for (int k = 0; k < decTi[i].size(); ++k)
          log("%s\n", decTi[i][k].c_str());
        log("...\n");
        for (int k = 0; k < defTi[j].size(); ++k)
          log("%s\n", defTi[j][k].c_str());
        continue;
      }
      if (decTo[i] != defTo[j]) 
        continue;
      ok = true;
    }
    if (!ok) {
      log("funkcija koja nije definirana: %s\n", declared[i].c_str());
      puts("funkcija");
      exit(0);
    }
  }

  return 0;
}
