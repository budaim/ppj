#!/bin/bash

for f in tp2/*.in
do
  echo "ide $f"
  ./analizator < $f > outf 2>/dev/null
  diff -qb outf ${f/.in/.out} 2>/dev/null
  if [ $? -ne '0' ]
  then
    echo "greska!                                     $f"
    cp $f ./test.in
    cp ${f/.in/.out} ./test.out
    cp ${f/.in/.c} ./test.c
  fi
done
