#include <cstdio>
#include <cstring>
#include <cassert>

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

#define log(...) fprintf(stderr, __VA_ARGS__)

// util

int indent(string s) {
  int ret = 0;
  for (int i = 0; i < s.size() && s[i] == ' '; ++i)
    ++ret;
  return ret;
}

string lstrip(string s) {
  int level = indent(s);
  return s.substr(level, s.size() - level);
}

string getUniform(string s) {
  s = lstrip(s);
  int i = find(s.begin(), s.end(), ' ') - s.begin();
  return s.substr(i, s.size() - i);
}

string getText(string s) {
  s = lstrip(s);
  int i = find(s.begin(), s.end(), ' ') - s.begin();
  i = find(s.begin() + i + 1, s.end(), ' ') - s.begin();
  return lstrip(s.substr(i, s.size() - i));
}

vector< string > csplit(string s) {
  string now = "";
  vector< string > ret;

  for (int i = 0; i < s.size(); ++i) {
    if (s[i] == ' ' || s[i] == ',') {
      if (now != "") ret.push_back(now);
      now = "";
    }
    else
      now += s[i];
  }

  ret.push_back(now);

  return ret;
}


#define NLEN 9

int num[NLEN + 1];
string uniqueString() {
  ++num[0];

  for (int i = 0; i < NLEN; ++i) {
    num[i + 1] += num[i] / 26;
    num[i] %= 26;
  }

  string ret = "";
  for (int i = 0; i < NLEN; ++i)
    ret += num[i] + 'A';

  return ret;
}

// input

vector< string > lines;
string line;

void read_input() {
  while (getline(cin, line)) 
    lines.push_back(line);
}

// tree parsing

int lindex;
vector< vector< int > > T;

int parse_tree() {
  int level = indent(lines[lindex]);
  int ret = lindex++;

  log("%d : %d\n", T.size(), ret);

  while (lindex < lines.size() && indent(lines[lindex]) > level) 
    T[ret].push_back(parse_tree());

  return ret;
}

string getNode(int n) {
  string ret = lstrip(lines[n]);
  ret = ret.substr(0, find(ret.begin(), ret.end(), ' ') - ret.begin());
  return ret;
}

string getProd(int n) {
  string ret = getNode(T[n][0]);
  for (int i = 1; i < T[n].size(); ++i)
    ret += " " + getNode(T[n][i]);
  return ret;
}

char buff[555];
string str(int v) {
  sprintf(buff, "0%X", v);
  return string(buff);
}

string dstr(int v) {
  sprintf(buff, "%d", v);
  return string(buff);
}

const string TT = "               ";

void stkApply(string cmd);

struct Snippet {
  string code;
  int cnt;
  int ecnt;
  int array;

  Snippet() { 
    cnt = 0;
    array = 0;
    code = ""; 
  }

  Snippet(string c) {
    cnt = 1;
    array = 0;
    code = c;
  }

  int size() { return cnt; }

  void addCommand(string cmd) {
    ++cnt;
    stkApply(cmd);
    code += TT + cmd + "\n";
  }

  void addLabeled(string cmd) {
    code += cmd + "\n";
  }
};

Snippet operator+(Snippet a, Snippet b) {
  Snippet ret(a.code + b.code);
  ret.cnt = a.cnt + b.cnt;
  ret.ecnt = a.ecnt + b.ecnt;
  ret.array = a.array + b.array;
  return ret;
}

struct NestingSnippet {
  Snippet h;
  Snippet t;

  NestingSnippet() {}
  NestingSnippet(Snippet _h, Snippet _t) {
    h = _h;
    t = _t;
  }

  Snippet toSnippet() {
    return h + t;
  }
};
  
Snippet operator+(NestingSnippet n, Snippet s) {
  return n.h + s + n.t;
}
  
NestingSnippet operator+(NestingSnippet n1, NestingSnippet n2) {
  return NestingSnippet(n1.h + n2.h, n2.t + n1.t);
}


NestingSnippet deactivator() {
  string ulabel = uniqueString();
  Snippet head(TT + "JP " + ulabel + "\n");
  Snippet tail(ulabel + "\n");
  return NestingSnippet(head, tail);
}

// function table
map< string, string > ft;
map< string, string > regs;
vector< string > stk;
int stk_cnt;

void stkLog() {
  log("STACK LOG -------------------------------\n");
  for (int i = 0; i < stk.size(); ++i)
    log("             %s\n", stk[i].c_str());
}

void stkPush(string reg) {
  //log("push %s na stack: %s\n", reg.c_str(), regs[reg].c_str());
  stk.push_back(regs[reg]);
  //stkLog();
}

void stkPop(string reg) {
  //log("pop %s sa stacka\n", reg.c_str());
  assert(stk.size() != 0);
  regs[reg] = stk.back();
  stk.pop_back();
  //stkLog();
}

void stkApply(string cmd) {
  vector< string > v = csplit(cmd);

  if (v[0] == "LOAD") {
    regs[v[1]] = "$$$";
  }

  if (v[0] == "MOVE") {
    regs[v[2]] = "$$$";
  }

  if (v[0] == "ADD" ||
      v[0] == "SUB" ||
      v[0] == "AND" ||
      v[0] == "OR" ||
      v[0] == "XOR") {
    regs[v[3]] = "$$$";
  }
}


int getGlobal(string id) {
  for (int i = 0; i < stk_cnt; ++i)
    if (stk[i] == id)
      return 0x40000 - i * 4 - 4;
}

int getRelative(string id) {
  log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!! trazim %s\n", id.c_str());
  for (int i = stk.size() - 1; i >= 0; --i)
    if (stk[i] == id)
      return (stk.size() - 1 - i) * 4;

  assert(false);
}

bool isGlobal(string id) {
  for (int i = stk.size() - 1; i >= stk_cnt; --i) 
    if (stk[i] == id)
      return false;
  return true;
}

string unfold(int n) {
  if (!T[n].size())
    return getNode(n);
  return unfold(T[n][0]);
}

string unfoldText(int n) {
  if (!T[n].size())
    return getText(lines[n]);
  return unfoldText(T[n][0]);
}

Snippet footer;

Snippet cIzraz(int n);
Snippet cDeklaracija(int n);

Snippet c(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  assert(false);
}

NestingSnippet cListaArgumenata(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<izraz_pridruzivanja>") {
    Snippet h = Snippet("; start liste argumenata\n") + cIzraz(T[n][0]);
    stkPop("R2");
    h.addCommand("POP R2");
    stkPop("R1");
    h.addCommand("POP R1");
    stkPush("R2");
    h.addCommand("PUSH R2");
    //log("!?!?!?!?! h vratio %s\n", h.code.c_str());
    Snippet t;
    t.addCommand("POP R4   ; nesting end"); // NA STK SE OVO RADI U *1
    return NestingSnippet(h, t);
  }

  if (prod == "<lista_argumenata> ZAREZ <izraz_pridruzivanja>") {
    NestingSnippet ns = cListaArgumenata(T[n][0]);
    Snippet ip = cIzraz(T[n][2]);
    stkPop("R2");
    ip.addCommand("POP R2");
    stkPop("R1");
    ip.addCommand("POP R1");
    stkPush("R2");
    ip.addCommand("PUSH R2");

    ns.h = ns.h + ip;
    
    Snippet t;
    t.addCommand("POP R4   ; nesting end"); // NA STK SE OVO RADI U *1

    ns.t = t + ns.t;

    return ns;
  }

  assert(false);
}

Snippet cIzraz(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "BROJ") {
    string val = getText(lines[T[n][0]]);

    string constant = uniqueString();

    Snippet ret;
    ret.addCommand("LOAD R1, (" + constant + ")");
    footer.addLabeled(constant + " DW \%D " + val);

    stkPush("R1");
    ret.addCommand("PUSH R1"); // kao adresa
    stkPush("R1");
    ret.addCommand("PUSH R1"); // vrijednost

    return ret;
  }
  
  if (prod == "ZNAK") {
    string val = getText(lines[T[n][0]]).substr(1, 1);
    log("znak %d\n", val[0]);

    string constant = uniqueString();

    Snippet ret;
    ret.addCommand("LOAD R1, (" + constant + ")");
    footer.addLabeled(constant + " DW \%D " + dstr(val[0]));

    stkPush("R1");
    ret.addCommand("PUSH R1"); // kao adresa
    stkPush("R1");
    ret.addCommand("PUSH R1"); // vrijednost

    return ret;
  }

  if (prod == "IDN") {
    Snippet ret;
    string name = getText(lines[T[n][0]]);
    if (!isGlobal(name)) {
      log("-------------------------------------------- %s nije globalno ime!!\n", name.c_str());
      int pos = getRelative(name);
      log("pozicija identifikatora %s je: %d\n", name.c_str(), pos);
      string address = "(SP+" + str(pos) + ") ; loadam " + name;
      ret.addCommand("MOVE SP, R1");
      ret.addCommand("ADD R1, " + str(pos) + ", R1");
      ret.addCommand("LOAD R2, " + address);
      stkPush("R1");
      ret.addCommand("PUSH R1");
      stkPush("R2");
      log("iz R2 pusham %s\n", stk.back().c_str());
      ret.addCommand("PUSH R2");
      return ret;
    }
    int globalPos = getGlobal(name);
    string address = str(globalPos);
    ret.addCommand("MOVE " + address + ", R1");
    ret.addCommand("LOAD R2, (" + address + ")");
    stkPush("R1");
    ret.addCommand("PUSH R1");
    stkPush("R2");
    ret.addCommand("PUSH R2");
    return ret;
  }

  if (prod == "<unarni_operator> <cast_izraz>") {
    Snippet ret = cIzraz(T[n][1]);
    string op = unfold(T[n][0]);

    log("operator je %s\n", op.c_str());

    if (op == "MINUS") {
      stkPop("R1");
      ret.addCommand("POP R1");
      ret.addCommand("MOVE 0, R2");
      ret.addCommand("SUB R2, R1, R1 ; unarni minus!!");
      stkPush("R1");
      ret.addCommand("PUSH R1");
      return ret;
    }

    assert(false);
  }

  if (prod == "OP_INC <unarni_izraz>") {
    Snippet ret = cIzraz(T[n][1]);
    stkPop("R1");
    ret.addCommand("POP R1"); // vrijednost
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addCommand("MOVE R1, R3 ; ide ++");
    ret.addCommand("ADD R3, 1, R3");
    ret.addCommand("STORE R3, (R2)"); // spremi se povecana vrijednost

    stkPush("R2");
    ret.addCommand("PUSH R2");
    stkPush("R3");
    ret.addCommand("PUSH R3"); 

    return ret;
  }
  
  if (prod == "<postfiks_izraz> OP_INC") {
    Snippet ret = cIzraz(T[n][0]);
    stkPop("R1");
    ret.addCommand("POP R1"); // vrijednost
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addCommand("MOVE R1, R3 ; ide ++");
    ret.addCommand("ADD R3, 1, R3");
    ret.addCommand("STORE R3, (R2)"); 

    stkPush("R2");
    ret.addCommand("PUSH R2");
    stkPush("R1");
    ret.addCommand("PUSH R1"); // ali vrati se stara

    return ret;
  }

  if (prod == "<postfiks_izraz> L_ZAGRADA D_ZAGRADA") {
    string name = unfoldText(T[n][0]);
    string label = ft[name];
    Snippet ret;
    /*

    for (int i = 0; i < stk_cnt; ++i) {
      string name = stk[i];
      int pos = 0x40000 - i * 4;
      ret.addCommand("LOAD R1, (" + str(pos) + ")");
      regs["R1"] = name;
      stkPush("R1");
      ret.addCommand("PUSH R1");
    }

    stkLog();
    */

    stkPush("R6");
    stkPush("R6"); // kao return vrijednost :)
    ret.addCommand("CALL " + label);
    stkPop("R2");
    ret.addCommand("POP R2"); // jel to return vrijednost :P
    stkPop("R1");
    ret.addCommand("POP R1");

    /*

    for (int i = stk_cnt - 1; i >= 0; --i) {
      stkPop("R3");
      ret.addCommand("POP R3");
      int pos = 0x40000 - i * 4;
      ret.addCommand("STORE R3, (" + str(pos) + ")");
    }

    stkLog();
    */

    stkPush("R1");
    ret.addCommand("PUSH R1"); // fake adresa
    stkPush("R2");
    ret.addCommand("PUSH R1");
    return ret;
  }

  if (prod == "<postfiks_izraz> L_ZAGRADA <lista_argumenata> D_ZAGRADA") {
    string name = unfoldText(T[n][0]);
    string label = ft[name];
    NestingSnippet args = cListaArgumenata(T[n][2]);
    Snippet ret;


    log("poziv funkcije, args ima snippet %s\n", args.h.code.c_str());
    ret.addCommand("CALL " + label + " ; call funkcije!!");
    log("ide CALL funkcije, na stacku je %d (ukljucuje argumente i rezultat)\n", stk.size());
    stkPush("R1");
    stkPush("R2");
    stkPop("R2");
    ret.addCommand("POP R2"); // je li to return vrijednost :P
    stkPop("R1");
    ret.addCommand("POP R1"); // rezultat funkcije
    log("args.t ima %d popova\n", args.t.size());
    for (int i = 0; i < args.t.size(); ++i) // POP ZA LISTU ARGUMENATA *1
      stkPop("R4");
    ret = args + ret;
    log("sad na stacku imam %d\n", stk.size());
    stkPush("R1");
    ret.addCommand("PUSH R1");
    stkPush("R1");
    ret.addCommand("PUSH R1");
    return ret;
  }

  if (prod == "<postfiks_izraz> L_UGL_ZAGRADA <izraz> D_UGL_ZAGRADA") {
    string name = unfoldText(T[n][0]);
    int pos = getRelative(name);

    Snippet ret = cIzraz(T[n][2]);
    stkPop("R2");
    ret.addCommand("POP R2");
    stkPop("R1");
    ret.addCommand("POP R1");

    ret.addCommand("ADD R2, R2, R2");
    ret.addCommand("ADD R2, R2, R2"); // * 4

    ret.addCommand("MOVE SP, R1");
    ret.addCommand("ADD R1, " + str(pos) + ", R1 ; za loadanje " + name); // R1 je sad adresa adrese pocetka niza

    ret.addCommand("LOAD R1, (R1)"); // R1 je sad adresa pocetka niza

    ret.addCommand("ADD R1, R2, R1"); //  R1 je sad adresa elementa koji se trazi

    ret.addCommand("LOAD R2, (R1)");

    stkPush("R1");
    ret.addCommand("PUSH R1 ; adresa za niz");
    stkPush("R2");
    ret.addCommand("PUSH R2 ; vrijednost u nizu");

    return ret;
  }

  if (prod == "L_ZAGRADA <ime_tipa> D_ZAGRADA <cast_izraz>") 
    return cIzraz(T[n][3]);

  if (prod == "<postfiks_izraz> OP_PRIDRUZI <izraz_pridruzivanja>") {
    Snippet ret = cIzraz(T[n][0]);
    ret = ret + cIzraz(T[n][2]);

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R3");
    ret.addCommand("POP R3");
    stkPop("R2");
    ret.addCommand("POP R2");
    stkPop("R3");
    ret.addCommand("POP R3");

    ret.addCommand("STORE R1, (R3)");
    stkPush("R3");
    ret.addCommand("PUSH R3");
    stkPush("R1");
    ret.addCommand("PUSH R1");

    return ret;
  }

  if (prod == "L_ZAGRADA <izraz> D_ZAGRADA") 
    return cIzraz(T[n][1]);

  if (prod == "<log_ili_izraz> OP_ILI <log_i_izraz>") {
    Snippet ret = cIzraz(T[n][0]);

    string label = uniqueString();
  
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addCommand("CMP R1, 0");
    ret.addCommand("JR_NE " + label);

    ret = ret + cIzraz(T[n][2]);

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addLabeled(label);

    stkPush("R1");
    ret.addCommand("PUSH R1");
    stkPush("R2");
    ret.addCommand("PUSH R2");

    return ret;
  }
  
  if (prod == "<log_i_izraz> OP_I <bin_ili_izraz>") {
    Snippet ret = cIzraz(T[n][0]);

    string label = uniqueString();
  
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addCommand("CMP R1, 0");
    ret.addCommand("JR_EQ " + label);

    ret = ret + cIzraz(T[n][2]);

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addLabeled(label);

    stkPush("R1");
    ret.addCommand("PUSH R1");
    stkPush("R2");
    ret.addCommand("PUSH R2");

    return ret;
  }

  if (T[n].size() == 3) {
    log("??????? krece operiranje, na stacku %d\n", stk.size());
    Snippet a = cIzraz(T[n][0]);
    log("??????? krece operiranje2, na stacku %d\n", stk.size());
    Snippet b = cIzraz(T[n][2]);
    log("??????? imam argumente, na stacku %d\n", stk.size());

    Snippet ret = a + b;

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R3");
    ret.addCommand("POP R3");
    stkPop("R2");
    ret.addCommand("POP R2");

    // ko jebe zadnju adresu

    string op = getNode(T[n][1]);
    log("??????????????? ide operator, na stacku je %d\n", stk.size());

    if (op == "PLUS") 
      ret.addCommand("ADD R2, R1, R1");
    else if (op == "MINUS")
      ret.addCommand("SUB R2, R1, R1 ; operacija s minusom");
    else if (op == "OP_BIN_ILI")
      ret.addCommand("OR R2, R1, R1");
    else if (op == "OP_BIN_I")
      ret.addCommand("AND R2, R1, R1");
    else if (op == "OP_BIN_XILI")
      ret.addCommand("XOR R2, R1, R1");
    else if (op == "OP_LT" || op == "OP_GT" || op == "OP_LTE" || op == "OP_GTE" || op == "OP_EQ") {
      string suffix = "S" + op.substr(3, op.size() - 3);
      if (suffix == "SLTE") suffix = "SLE";
      if (suffix == "SGTE") suffix = "SGE";
      if (suffix == "SEQ") suffix = "Z";

      ret.addCommand("MOVE R1, R3");
      ret.addCommand("MOVE 0, R1");
      ret.addCommand("CMP R2, R3 ; operator usporedbe!");

      string label1 = uniqueString();
      string label2 = uniqueString();

      ret.addCommand("JR_" + suffix + " " + label1);
      ret.addCommand("JR " + label2);

      ret.addLabeled(label1);
      ret.addCommand("MOVE 1, R1");
      ret.addLabeled(label2);
    }
    else if (op == "OP_MOD") {
      // R2 mod R1

      string start = uniqueString();
      string finish = uniqueString();

      ret.addLabeled(start);
      ret.addCommand("CMP R2, R1");
      ret.addCommand("JP_SLT " + finish);

      ret.addCommand("SUB R2, R1, R2");

      ret.addLabeled(finish);

      ret.addCommand("MOVE R2, R1");
    }
    else {
      log("%s\n", prod.c_str());
      assert(false);
    }

    stkPush("R1");
    ret.addCommand("PUSH R1");
    log("??????????????? nakon operatora, na stacku je %d\n", stk.size());

    return ret;
  }

  if (T[n].size() == 1) {
    return cIzraz(T[n][0]);
  }

  assert(false);
}

Snippet cNaredbaSkoka(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "KR_RETURN <izraz> TOCKAZAREZ") {
    Snippet izraz = cIzraz(T[n][1]);
    stkPop("R1");
    izraz.addCommand("POP R1 ; naredba skoka");
    stkPop("R2");
    izraz.addCommand("POP R2");
    stkPush("R1");
    izraz.addCommand("PUSH R1");
    izraz.addCommand("MOVE 1, R1");
    stkPush("R1");
    izraz.addCommand("PUSH R1");
    return izraz;
  }

  assert(false);
}

Snippet cNaredba(int n);
Snippet cSlozenaNaredba(int n);

Snippet cNaredbaGrananja(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba>") {
    Snippet ret = cIzraz(T[n][2]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2"); // adresa

    string finish = uniqueString();
    string jump = uniqueString();

    ret.addCommand("CMP R1, 0 ; provjera izraza u ifu");
    ret.addCommand("JP_EQ " + jump);

    ret = ret + cNaredba(T[n][4]);
    ret.addCommand("JP " + finish);

    ret.addLabeled(jump + " ; jump u obicnom ifu");

    stkPop("R0");
    stkPop("R0"); // ostatak od naredbe koja nije ni izvrsena :S

    stkPush("R1");
    ret.addCommand("PUSH R1");
    ret.addCommand("MOVE 0, R1");
    stkPush("R1");
    ret.addCommand("PUSH R1");

    ret.addLabeled(finish + " ; finish u obicnom ifu");

    return ret;
  }

  if (prod == "KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba> KR_ELSE <naredba>") {
    Snippet ret = cIzraz(T[n][2]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2"); // adresa

    string finish = uniqueString();
    string jump = uniqueString();

    ret.addCommand("CMP R1, 0 ; provjera izraza u ifu");
    ret.addCommand("JP_EQ " + jump);
    
    ret = ret + cNaredba(T[n][4]);
    ret.addCommand("JP " + finish);

    ret.addLabeled(jump + " ; jump u obicnom ifu");

    stkPop("R0");
    stkPop("R0"); // ostatak od naredbe koja nije ni izvrsena :S

    ret = ret + cNaredba(T[n][6]);

    ret.addLabeled(finish + " ; finish u obicnom ifu");

    return ret;
  }

  assert(false);
}

Snippet cIzrazNaredba(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<izraz> TOCKAZAREZ") {
    Snippet ret = cIzraz(T[n][0]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");
    stkPush("R1");
    ret.addCommand("PUSH R1");
    ret.addCommand("MOVE 0, R1");
    stkPush("R1");
    ret.addCommand("PUSH R1");
    return ret;
  }

  assert(false);
}

Snippet cNaredbaPetlje(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "KR_FOR L_ZAGRADA <izraz_naredba> <izraz_naredba> <izraz> D_ZAGRADA <naredba>") {
    Snippet ret = cIzrazNaredba(T[n][2]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R1");
    ret.addCommand("POP R1");

    string loop = uniqueString();
    string stop = uniqueString();

    ret.addLabeled(loop);

    ret = ret + cIzrazNaredba(T[n][3]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2"); // vrijednost izraza :P

    ret.addCommand("CMP R2, 0");
    ret.addCommand("JP_EQ " + stop);

    ret = ret + cNaredba(T[n][6]);

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addCommand("CMP R1, 0"); // 1 == return!!
    ret.addCommand("JP_NE " + stop);

    ret = ret + cIzraz(T[n][4]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R1");
    ret.addCommand("POP R1");

    ret.addCommand("JP " + loop);
    ret.addLabeled(stop);

    string label = uniqueString();
    
    stkPush("R2");
    ret.addCommand("PUSH R2");
    stkPush("R1");
    ret.addCommand("PUSH R1");

    ret.addCommand("CMP R1, 1");
    ret.addCommand("JR_EQ " + label);

    stkPop("R1");
    ret.addCommand("POP R1");
    ret.addCommand("MOVE 0, R1");
    stkPush("R1");
    ret.addCommand("PUSH R1");

    ret.addLabeled(label);

    return ret;
  }

  if (prod == "KR_WHILE L_ZAGRADA <izraz> D_ZAGRADA <naredba>") {
    Snippet ret;
    string loop = uniqueString();
    string stop = uniqueString();

    ret.addLabeled(loop);

    ret = ret + cIzraz(T[n][2]);
    stkPop("R1");
    ret.addCommand("POP R1"); // vrijednost
    stkPop("R2");
    ret.addCommand("POP R2"); // adresa

    ret.addCommand("CMP R1, 0");
    ret.addCommand("JP_EQ " + stop);

    ret = ret + cNaredba(T[n][4]);

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");

    ret.addCommand("CMP R1, 0"); // 1 == return!!
    ret.addCommand("JP_NE " + stop);

    ret.addCommand("JP " + loop);
    ret.addLabeled(stop);

    string label = uniqueString();
    
    stkPush("R2");
    ret.addCommand("PUSH R2");
    stkPush("R1");
    ret.addCommand("PUSH R1");

    ret.addCommand("CMP R1, 1");
    ret.addCommand("JR_EQ " + label);

    stkPop("R1");
    ret.addCommand("POP R1");
    ret.addCommand("MOVE 0, R1");
    stkPush("R1");
    ret.addCommand("PUSH R1");

    ret.addLabeled(label);

    return ret;
  }

  assert(false);
}

Snippet cNaredba(int n) {
  string prod = getProd(n);
  string node = getNode(n);
  log("%s\n", prod.c_str());
  log("%s\n", node.c_str());

  if (prod == "<naredba_skoka>") 
    return cNaredbaSkoka(T[n][0]);

  if (prod == "<slozena_naredba>")
    return cSlozenaNaredba(T[n][0]);

  if (prod == "<naredba_grananja>")
    return cNaredbaGrananja(T[n][0]);

  if (prod == "<izraz_naredba>")
    return cIzrazNaredba(T[n][0]);

  if (prod == "<naredba_petlje>")
    return cNaredbaPetlje(T[n][0]);

  assert(false);
}

Snippet cListaNaredbi(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<naredba>") {
    Snippet ret = cNaredba(T[n][0]);

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");
    stkPop("R3");
    ret.addCommand("POP R3");

    ret.addCommand("CMP R1, 0");

    string label = uniqueString();

    ret.addCommand("JR_EQ " + label);
    stkPush("R2");
    ret.addCommand("PUSH R2");
    stkPush("R1");
    ret.addCommand("PUSH R1");
    stkPush("R3");
    ret.addCommand("PUSH R3");
    ret.addCommand("RET");

    ret.addLabeled(label);
    stkPop("R0");
    stkPop("R0");
    stkPop("R0"); // push za neizvrseni dio

    stkPush("R3");
    ret.addCommand("PUSH R3");

    return ret;
  }

  if (prod == "<lista_naredbi> <naredba>") {
    Snippet ret = cListaNaredbi(T[n][0]);
    ret = ret + cNaredba(T[n][1]);

    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");
    stkPop("R3");
    ret.addCommand("POP R3");

    ret.addCommand("CMP R1, 0");

    string label = uniqueString();

    ret.addCommand("JR_EQ " + label);
    stkPush("R2");
    ret.addCommand("PUSH R2");
    stkPush("R1");
    ret.addCommand("PUSH R1");
    stkPush("R3");
    ret.addCommand("PUSH R3");
    ret.addCommand("RET");

    ret.addLabeled(label);
    stkPop("R0");
    stkPop("R0");
    stkPop("R0"); // push za neizvrseni dio

    stkPush("R3");
    ret.addCommand("PUSH R3");

    return ret;
  }

  assert(false);
}

NestingSnippet cListaDeklaracija(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<deklaracija>") {
    Snippet h = cDeklaracija(T[n][0]);
    Snippet t;
    log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! deklaracija od %d popova\n", h.ecnt + h.array);
    for (int i = 0; i < h.ecnt + h.array; ++i)
      t.addCommand("POP R3 ; pop od deklaracije"); // OVO SE RIJESI U *2
    return NestingSnippet(h, t);
  }

  if (prod == "<lista_deklaracija> <deklaracija>") {
    NestingSnippet ns = cListaDeklaracija(T[n][0]);
    Snippet h = cDeklaracija(T[n][1]);
    log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! deklaracija od %d popova\n", h.ecnt + h.array);
    Snippet t;
    for (int i = 0; i < h.ecnt + h.array; ++i)
      t.addCommand("POP R3 ; pop od deklaracije"); // OVO SE RIJESI U *2
    return ns + NestingSnippet(h, t);
  }

  assert(false);
}

Snippet cSlozenaNaredba(int n) {
  string prod = getProd(n);

  if (prod == "L_VIT_ZAGRADA <lista_naredbi> D_VIT_ZAGRADA") {
    regs["R5"] = "$SZS";
    stkPush("R5");
    regs["R5"] = "$$$";
    Snippet ln = cListaNaredbi(T[n][1]);
    stkPop("R5");
    string label = uniqueString();
    ln.code = label + " ; slozena naredba!! \n" + ln.code;
    stkPop("R3");
    ln.addCommand("POP R3 ; za adresu neku...");
    stkPush("R1");
    ln.addCommand("PUSH R1");
    ln.addCommand("MOVE 0, R1");
    stkPush("R1");
    ln.addCommand("PUSH R1");
    stkPush("R3");
    ln.addCommand("PUSH R3");
    ln.addCommand("RET");
    ln = deactivator() + ln;
    ln.addCommand("CALL " + label);
    
    Snippet pref("         ; pocetak slozene naredbe\n");
    Snippet suff("              ; kraj slozene naredbe\n");
    return pref + ln + suff;
  }

  if (prod == "L_VIT_ZAGRADA <lista_deklaracija> <lista_naredbi> D_VIT_ZAGRADA") {
    NestingSnippet ld = cListaDeklaracija(T[n][1]);
    
    stkPush("R5");
    Snippet ln = cListaNaredbi(T[n][2]);
    stkPop("R5");
    string label = uniqueString();
    ln.code = label + " ; slozena naredba!! \n" + ln.code;
    stkPop("R3");
    ln.addCommand("POP R3 ; za adresu neku...");
    stkPush("R1");
    ln.addCommand("PUSH R1");
    ln.addCommand("MOVE 0, R1");
    stkPush("R1");
    ln.addCommand("PUSH R1");
    stkPush("R3");
    ln.addCommand("PUSH R3");
    ln.addCommand("RET");
    ln = deactivator() + ln;
    ln.addCommand("CALL " + label);

    stkPop("R1");
    ln.addCommand("POP R1");
    stkPop("R2");
    ln.addCommand("POP R2");

    Snippet ret = ld + ln;

    log("imam za popati %d\n", ld.h.ecnt);
    for (int i = 0; i < ld.h.ecnt + ld.h.array; ++i)
      stkPop("R0");

    stkPush("R2");
    ret.addCommand("PUSH R2");
    stkPush("R2");
    ret.addCommand("PUSH R1");
    
    Snippet pref("         ; pocetak slozene naredbe\n");
    Snippet suff("              ; kraj slozene naredbe\n");

    return pref + ret + suff;
  }

  log("%s\n", prod.c_str());
  assert(false);
}

string cDeklaracijaParametra(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<ime_tipa> IDN" ||
      prod == "<ime_tipa> IDN L_UGL_ZAGRADA D_UGL_ZAGRADA") 
    return getText(lines[T[n][1]]);

  assert(false);
}

vector< string > cListaParametara(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  vector< string > ret;

  if (prod == "<deklaracija_parametra>") {
    ret.push_back(cDeklaracijaParametra(T[n][0]));
    return ret;
  }

  if (prod == "<lista_parametara> ZAREZ <deklaracija_parametra>") {
    ret = cListaParametara(T[n][0]);
    ret.push_back(cDeklaracijaParametra(T[n][2]));
    return ret;
  }

  assert(false);
}

Snippet cDefinicijaFunkcije(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<ime_tipa> IDN L_ZAGRADA KR_VOID D_ZAGRADA <slozena_naredba>") {
    string name = getText(lines[T[n][1]]);
    string label = "";

    if (ft.count(name) == 0) 
      label = ft[name] = uniqueString();
    else
      label = ft[name];

    stkPush("R5");
    Snippet sn = cSlozenaNaredba(T[n][5]);
    stkPop("R1");
    sn.addCommand("POP R1");
    stkPop("R2");
    sn.addCommand("POP R2");
    stkPop("R3");
    sn.addCommand("POP R3");
    stkPush("R2");
    sn.addCommand("PUSH R2");
    stkPush("R1");
    sn.addCommand("PUSH R1");
    stkPush("R3");
    sn.addCommand("PUSH R3");
    sn.addCommand("RET");

    Snippet ret = deactivator() + (Snippet(label + "   ; funkcija " + name + "\n") + sn);
    stkPop("R5");
    stkPop("R0");
    stkPop("R0");

    return ret;
  }

  if (prod == "<ime_tipa> IDN L_ZAGRADA <lista_parametara> D_ZAGRADA <slozena_naredba>") {
    string name = getText(lines[T[n][1]]);
    string label = "";

    if (ft.count(name) == 0) 
      label = ft[name] = uniqueString();
    else
      label = ft[name];

    vector< string > params = cListaParametara(T[n][3]);

    for (int i = 0; i < params.size(); ++i) {
      regs["R6"] = params[i];
      stkPush("R6");
    }
    
    stkPush("R5");
    log("??????????? prije slozene naredbe %d\n", stk.size());
    Snippet sn = cSlozenaNaredba(T[n][5]);
    stkPop("R1");
    sn.addCommand("POP R1");
    stkPop("R2");
    sn.addCommand("POP R2");
    stkPop("R3");
    sn.addCommand("POP R3");
    stkPush("R2");
    sn.addCommand("PUSH R2");
    stkPush("R1");
    sn.addCommand("PUSH R1");
    stkPush("R3");
    sn.addCommand("PUSH R3");
    sn.addCommand("RET");
    Snippet ret = deactivator() + (Snippet(label + "   ; funkcija " + name + "\n") + sn);
    stkPop("R5");
    stkPop("R0");
    stkPop("R0");
    
    for (int i = 0; i < params.size(); ++i) 
      stkPop("R0");
   
    return ret; 
  }

  assert(false);
}

Snippet cInicijalizator(int n);

Snippet cIzravniDeklarator(int n) {
  string prod = getProd(n);
  string node = getNode(n);
  log("%s\n", prod.c_str());
  log("%s\n", node.c_str());

  if (prod == "IDN") {
    Snippet ret;
    ret.ecnt = 1;
    string name = getText(lines[T[n][0]]);
    regs["R1"] = name;
    stkPush("R1");
    ret.addCommand("PUSH R1");
    return ret;
  }

  if (prod == "IDN L_UGL_ZAGRADA BROJ D_UGL_ZAGRADA") {
    Snippet ret;
    ret.array = 1;
    int cnt = atoi(getText(lines[T[n][2]]).c_str());
    ret.ecnt = cnt;

    // prazne vrijednosti
    for (int i = 0; i < cnt; ++i) {
      stkPush("R1");
      ret.addCommand("PUSH R1 ; deklaracija niza");
    }

    string name = getText(lines[T[n][0]]);
    ret.addCommand("MOVE SP, R1");
    regs["R1"] = name;
    stkPush("R1");
    ret.addCommand("PUSH R1");
    return ret;
  }

  assert(false);
}

Snippet cListaIzrazaPridruzivanja(int n, int shift) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<lista_izraza_pridruzivanja> ZAREZ <izraz_pridruzivanja>") {
    // ovo na stack ide naopako!!
    Snippet ret = cIzraz(T[n][2]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");
    /*
    stkPush("R1");
    ret.addCommand("PUSH R1");
    */

    ret.addCommand("STORE R1, (SP+" + str(shift) + ") ; popunjavanje niza");

    ret = ret + cListaIzrazaPridruzivanja(T[n][0], shift - 4);
    return ret;
  }

  if (prod == "<izraz_pridruzivanja>") {
    Snippet ret = cIzraz(T[n][0]);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");
    
    /*
    stkPush("R1");
    ret.addCommand("PUSH R1");
    */
    ret.addCommand("STORE R1, (SP+" + str(shift) + ")");

    return ret;
  }
  
  assert(false);
}

Snippet cInicijalizator(int n, int shift) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<izraz_pridruzivanja>") {
    Snippet ret = cIzraz(T[n][0]);
    assert(stk.size() >= 2);
    stkPop("R1");
    ret.addCommand("POP R1");
    stkPop("R2");
    ret.addCommand("POP R2");
    /*
    stkPush("R1");
    ret.addCommand("PUSH R1");
    */
    ret.addCommand("STORE R1, (SP+" + str(shift) + ")");
    return ret;
  }

  if (prod == "L_VIT_ZAGRADA <lista_izraza_pridruzivanja> D_VIT_ZAGRADA") 
    return cListaIzrazaPridruzivanja(T[n][1], shift);

  assert(false);
}

Snippet cInitDeklarator(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());
  
  if (prod == "<izravni_deklarator>") 
    return cIzravniDeklarator(T[n][0]);

  if (prod == "<izravni_deklarator> OP_PRIDRUZI <inicijalizator>") {
    Snippet id = cIzravniDeklarator(T[n][0]);
    string name = stk.back();
    Snippet in = cInicijalizator(T[n][2], getRelative(name) + 4 * id.ecnt - 4 * !id.array);

    log("gotova oba\n");
    
    Snippet ret = id + in;
    ret.ecnt = id.ecnt;
    ret.array = id.array;

    return ret;
  }

  assert(false);
}

Snippet cListaInitDeklaratora(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<init_deklarator>") 
    return cInitDeklarator(T[n][0]);
  
  assert(false);
}

Snippet cDeklaracija(int n) {
  string node = getNode(n);
  string prod = getProd(n);
  log("%s\n", node.c_str());
  log("%s\n", prod.c_str());

  if (prod == "<ime_tipa> <lista_init_deklaratora> TOCKAZAREZ") 
    return cListaInitDeklaratora(T[n][1]);

  //if (prod == "<>")

  assert(false);
}

Snippet cVanjskaDeklaracija(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<definicija_funkcije>") 
    return cDefinicijaFunkcije(T[n][0]);

  if (prod == "<deklaracija>")
    return cDeklaracija(T[n][0]);

  assert(false);
}

Snippet cPrijevodnaJedinica(int n) {
  string prod = getProd(n);
  log("%s\n", prod.c_str());

  if (prod == "<vanjska_deklaracija>") 
    return cVanjskaDeklaracija(T[n][0]);

  if (prod == "<prijevodna_jedinica> <vanjska_deklaracija>") {
    Snippet pj = cPrijevodnaJedinica(T[n][0]);
    Snippet vd = cVanjskaDeklaracija(T[n][1]);
    return pj + vd;
  }

  assert(false);
}

int main(void)
{
  read_input();
  T.resize(lines.size());
  parse_tree();

  for (int i = 1; i <= 7; ++i)
    regs["R" + dstr(i)] = "$$$";

  /*
  regs["R1"] = "$ZZZZ";
  stkPush("R1");
  regs["R1"] = "$$$";
  */

  Snippet program = Snippet(TT + "MOVE 40000, R7\n") + cPrijevodnaJedinica(0);

  stk_cnt = stk.size();
  log("na stacku ima ............................................................ %d\n", stk_cnt);
  stkLog();
  
  ft.clear();
  stk.clear();
  footer = Snippet();
  memset(num, 0, sizeof num);

  for (int i = 1; i <= 7; ++i)
    regs["R" + dstr(i)] = "$$$";

  /*
  regs["R1"] = "$ZZZZ";
  stkPush("R1");
  regs["R1"] = "$$$";
  */
  
  program = Snippet(TT + "MOVE 40000, R7\n") + cPrijevodnaJedinica(0);

  program.addCommand("CALL " + ft["main"]);
  program.addCommand("POP R6");
  program.addCommand("POP R6");
  program.addCommand("HALT");

  program = program + footer;

  printf("%s\n", program.code.c_str());
  
  return 0;
}
