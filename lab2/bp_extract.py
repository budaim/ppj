def escaped(s):
  ret = ""
  for c in s:
    if c in ['"', '\\']:
      ret += '\\' + c
    else: 
      ret += c
  return ret

analizator_bp = open('analizator_bp.cpp')
generator_bp = open('generator_bp.cpp')

bp1 = True
for i in generator_bp:
  if "// ==" in i:
    bp1 = not bp1
    if bp1:
      out = True
      print "const char bp[] =",
      for line in analizator_bp:
        if "// ==" in line:
          out = not out
        elif out:
          if not line:
            continue
          print "\n  \"" + escaped(line.rstrip()) + "\\n\"",
      print ";"
    print i,
  elif bp1:
    print i,

